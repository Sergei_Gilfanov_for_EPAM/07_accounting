package com.epam.javatraining2016.accountsInFiles;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.CoreMatchers.*;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.javatraining2016.accounts.AccountAlreadyExists;
import com.epam.javatraining2016.accounts.AccountOwner;
import com.epam.javatraining2016.accounts.account.Account;
import com.epam.javatraining2016.accounts.account.AccountLoader;
import com.epam.javatraining2016.accounts.account.AccountWriter;
import com.epam.javatraining2016.accounts.base.AccountsDb;

import static org.mockito.Mockito.*;



@RunWith(MockitoJUnitRunner.class)
public class AccountTest {

  @Before
  public void setUp() throws Exception {}

  @Test
  @Ignore
  public void testWriteAndRead() throws IOException, AccountAlreadyExists, ClassNotFoundException {
    AccountsDb db = mock(AccountsDb.class);
    String ownerName = "Asdfgh";
    AccountOwner owner = new AccountOwner(ownerName);
    String accountId = "76f0b7c3-18bb-4b0f-84b7-6c8755fe5968";
    long balance = 123467;
    
    Account account = new Account(db, accountId, owner);
    account.setBalance(balance);
  
    Path path = Files.createTempFile(accountId, ".acc");

    AccountWriter writer = new AccountWriter(path, account);
    Exception res = writer.call();
    assertThat(res, is(nullValue()));
    
    AccountLoader loader = new AccountLoader(path, db);
    Account accountLoaded = loader.call();
    assertThat(accountLoaded.getAccountId(), equalTo(accountId));
    assertThat(accountLoaded.getBalance(), equalTo(balance));
    assertThat(accountLoaded.getOwner().getName(), equalTo(ownerName));
  }

}
