package com.epam.javatraining2016.util;

import com.epam.javatraining2016.accounts.account.Account;
import com.epam.javatraining2016.accounts.base.ParsistentAccountsDbInterface;

public class Stat {
  public static void printListAndTotal(ParsistentAccountsDbInterface db) {
    long total = 0;
    for (Account account: db.getAccounts()) {
      long balance = account.getBalance();
      System.out.format("%-40s%d%n", account.getAccountId(), balance);
      total += balance;
    }
    System.out.println("---------------");
    System.out.format("%-40s%d%n", "Total", total);
  }

  public static void printTotal(ParsistentAccountsDbInterface db) {
    long total = 0;
    for (Account account: db.getAccounts()) {
      long balance = account.getBalance();
      total += balance;
    }
    System.out.format("%-40s%d%n", "Total", total);
  }
}
