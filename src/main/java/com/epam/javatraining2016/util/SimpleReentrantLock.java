package com.epam.javatraining2016.util;

// Наверное можно было бы использовать системный Lock, но нам не нужны
// все его методы.
public interface SimpleReentrantLock {
  public void lock();

  public void unlock();
}
