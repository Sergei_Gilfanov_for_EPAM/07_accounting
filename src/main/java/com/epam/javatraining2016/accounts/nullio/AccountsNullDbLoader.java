package com.epam.javatraining2016.accounts.nullio;

import java.io.IOException;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentSkipListMap;

import com.epam.javatraining2016.accounts.account.Account;
import com.epam.javatraining2016.accounts.base.AccountsDbLoaderInterface;
import com.epam.javatraining2016.accounts.base.ParsistentAccountsDbInterface;

public class AccountsNullDbLoader implements AccountsDbLoaderInterface {

  public AccountsNullDbLoader() {}

  @Override
  public ConcurrentMap<String, Account> load(ParsistentAccountsDbInterface db) throws IOException {
    return new ConcurrentSkipListMap<String, Account>();
  }

}
