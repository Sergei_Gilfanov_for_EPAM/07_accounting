package com.epam.javatraining2016.accounts.nullio;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epam.javatraining2016.accounts.account.Account;
import com.epam.javatraining2016.accounts.base.AccountsDbWriterInterface;

public class AccountsNullDbWriter implements AccountsDbWriterInterface {
  private static final Logger log = LoggerFactory.getLogger(AccountsNullDbWriter.class);

  @Override
  public void close() {
    log.info("Closed");
  }

  @Override
  public void write(Account account) {
    log.debug("Write '{}'. Balance = {}", account.getAccountId(), account.getBalance());
  }

}
