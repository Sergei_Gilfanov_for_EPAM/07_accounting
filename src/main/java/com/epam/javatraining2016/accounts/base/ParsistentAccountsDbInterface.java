package com.epam.javatraining2016.accounts.base;

import java.util.Collection;

import com.epam.javatraining2016.accounts.AccountAlreadyExists;
import com.epam.javatraining2016.accounts.AccountOwner;
import com.epam.javatraining2016.accounts.account.Account;
import com.epam.javatraining2016.accounts.account.AccountInterface;

public interface ParsistentAccountsDbInterface extends AccountsDbInterface {
  void write(Account account);

  /**
   * Записывает кеши и буферы базы, после чего закрывает базу данных для изменений.
   * 
   * @param path место расположения постоянного хранилища
   */
  void close();

  public int getSize();
  
  Collection<Account> getAccounts();

  public Account createAccount(String accountId, AccountOwner owner) throws AccountAlreadyExists;

  public Account createAccount(String accountId, AccountOwner owner, long balance)
      throws AccountAlreadyExists;

  public Account put(AccountInterface account) throws AccountAlreadyExists;
}
