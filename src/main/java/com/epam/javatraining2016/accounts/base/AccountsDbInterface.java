package com.epam.javatraining2016.accounts.base;

import com.epam.javatraining2016.accounts.AccountAlreadyExists;
import com.epam.javatraining2016.accounts.AccountDoesNotExists;
import com.epam.javatraining2016.accounts.AccountOwner;
import com.epam.javatraining2016.accounts.account.AccountInterface;

public interface AccountsDbInterface {
  public AccountInterface getAccount(String accountId) throws AccountDoesNotExists;

  public AccountInterface createAccount(String accountId, AccountOwner owner)
      throws AccountAlreadyExists;
}
