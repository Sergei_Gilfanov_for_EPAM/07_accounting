package com.epam.javatraining2016.accounts.base;

import java.io.IOException;
import java.util.Collection;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentSkipListMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epam.javatraining2016.accounts.AccountAlreadyExists;
import com.epam.javatraining2016.accounts.AccountDoesNotExists;
import com.epam.javatraining2016.accounts.AccountOwner;
import com.epam.javatraining2016.accounts.AccountingException;
import com.epam.javatraining2016.accounts.account.Account;
import com.epam.javatraining2016.accounts.account.AccountInterface;

public class AccountsDb implements ParsistentAccountsDbInterface {
  private static final Logger log = LoggerFactory.getLogger(AccountsDb.class);

  // Основная таблица счетов
  private boolean building; // Флажок, означающий, что база еще строится и ее изменения не надо писать не диск
  private ConcurrentMap<String, Account> accounts;
  private boolean closed;
  AccountsDbWriterInterface writer;


  public AccountsDb(AccountsDbLoaderInterface loader, AccountsDbWriterInterface writer)
      throws IOException, ClassNotFoundException, AccountingException {
    log.info("AccountsDb constructor - start");
    building = true;
    closed = false;
    accounts = new ConcurrentSkipListMap<String, Account>();
    this.writer = writer;
    loader.load(this);
    building = false;
    log.info("AccountsDb constructor - end");
  }
  public int getSize() {
    return accounts.size();
  }
  
  @Override
  public AccountInterface getAccount(String accountId) throws AccountDoesNotExists {
    AccountInterface retval = accounts.get(accountId);
    if (retval == null) {
      throw new AccountDoesNotExists("Unknown id" + accountId);
    }
    return retval;
  }

  @Override
  public void close() {
    closed = true;
    writer.close();
    log.info("Closed");
  }

  @Override
  public void write(Account account) {
    if (closed) {
      throw new IllegalStateException("write() on closed object");
    }
    if (!building) {
      writer.write(account);
    }
  }


  @Override
  public Account createAccount(String accountId, AccountOwner owner) throws AccountAlreadyExists {
  return createAccount(accountId, owner, 0);
  }
  
  @Override
  public Account createAccount(String accountId, AccountOwner owner, long balance) throws AccountAlreadyExists {
    Account newAccount = new Account(this, accountId, owner);
    newAccount.setBalance(balance);
    Account putResult = accounts.putIfAbsent(accountId, newAccount);
    if (putResult != null) {
      throw new AccountAlreadyExists(String.format("Account id %s already exists", accountId));
    }
    if (! building) {
      write(newAccount);
    }
    return newAccount;
  }
  
  @Override
  public Collection<Account> getAccounts() {
    return accounts.values();
  }

  @Override
  public Account put(AccountInterface account) throws AccountAlreadyExists {
    String accountId = account.getAccountId();
    Account newAccount = new Account(this, account.getAccountId(), account.getOwner());
    newAccount.setBalance(account.getBalance());
    Account putResult = accounts.putIfAbsent(accountId, newAccount);
    if (putResult != null) {
      throw new AccountAlreadyExists(String.format("Account id %s already exists", accountId));
    }
    return newAccount;
  }




}
