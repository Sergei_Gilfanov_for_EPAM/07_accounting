package com.epam.javatraining2016.accounts.base;

import java.io.IOException;
import java.util.concurrent.ConcurrentMap;

import com.epam.javatraining2016.accounts.AccountingException;
import com.epam.javatraining2016.accounts.account.Account;

public interface AccountsDbLoaderInterface {
  public ConcurrentMap<String, Account> load(ParsistentAccountsDbInterface accountsDb)
      throws IOException, AccountingException, ClassNotFoundException;
}
