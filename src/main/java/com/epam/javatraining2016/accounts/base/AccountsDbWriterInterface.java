package com.epam.javatraining2016.accounts.base;

import com.epam.javatraining2016.accounts.account.Account;

public interface AccountsDbWriterInterface {
  // Отключает метод write и записывает весь накопленный буфер записи. Блокирующий - ждет окончания
  // записи.
  void close();

  void write(Account account);
}
