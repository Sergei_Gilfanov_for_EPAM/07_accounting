package com.epam.javatraining2016.accounts;

public class AccountAlreadyExists extends AccountingException {
  private static final long serialVersionUID = 4766957971815798368L;

  public AccountAlreadyExists(String msg) {
    super(msg);
  }
}
