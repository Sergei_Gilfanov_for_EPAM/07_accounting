package com.epam.javatraining2016.accounts.instantio;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentSkipListMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epam.javatraining2016.accounts.AccountingException;
import com.epam.javatraining2016.accounts.account.Account;
import com.epam.javatraining2016.accounts.account.AccountLoader;
import com.epam.javatraining2016.accounts.base.AccountsDbLoaderInterface;
import com.epam.javatraining2016.accounts.base.ParsistentAccountsDbInterface;

public class AccountsInstantDbLoader implements AccountsDbLoaderInterface {
  private static final Logger log = LoggerFactory.getLogger(AccountsInstantDbLoader.class);
  Path dirToLoad;

  public AccountsInstantDbLoader(Path dbDir) {
    dirToLoad = dbDir;
  }
  @Override
  public ConcurrentMap<String, Account> load(ParsistentAccountsDbInterface db)
      throws IOException, AccountingException, ClassNotFoundException {
    log.info("loading - start");
    ConcurrentMap<String, Account> retval = new ConcurrentSkipListMap<String, Account>();

    try (DirectoryStream<Path> stream = Files.newDirectoryStream(dirToLoad, "*")) {
      for (Path entry : stream) {
        AccountLoader reader = new AccountLoader(entry, db);
        Account account = reader.call();
        retval.put(account.getAccountId(), account);
        log.debug("Reading of {} - done", account.getAccountId());
      }
    } catch (IOException ex) {
      log.error("Can't scan {}: {}", dirToLoad, ex);
      throw ex;
    }
    log.info("loading - end");
    return retval;
  }
}
