package com.epam.javatraining2016.accounts.account;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AccountWriter implements Callable<Exception> {
  private static final Logger log = LoggerFactory.getLogger(AccountWriter.class);
  Path writeWhere;
  Account writeWhat;

  public AccountWriter(Path writeWhere, Account writeWhat) {
    this.writeWhere = writeWhere;
    this.writeWhat = writeWhat;
  }

  @Override
  public Exception call() {
    log.info("Account writing({}) - start", writeWhat.getAccountId());
    Exception retval = null;
    writeWhat.lock();
    try (
        OutputStream os =
            Files.newOutputStream(writeWhere, StandardOpenOption.WRITE, StandardOpenOption.CREATE);
        BufferedOutputStream bos = new BufferedOutputStream(os);
        ObjectOutputStream oos = new ObjectOutputStream(bos)) {
      oos.writeObject(writeWhat);
      oos.flush();
      oos.close();
      log.debug("Writing of {} - done. Balance = {}", writeWhat.getAccountId(), writeWhat.getBalance());
    } catch (IOException ex) {
      log.error("Can't write account to {}: {}", writeWhere, ex);
      retval = ex;
    }
    writeWhat.unlock();
    log.info("Account writing({}) - end", writeWhat.getAccountId());
    return retval;
  }
}
