package com.epam.javatraining2016.accounts.account;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epam.javatraining2016.accounts.AccountAlreadyExists;
import com.epam.javatraining2016.accounts.base.ParsistentAccountsDbInterface;

public class AccountLoader implements Callable<Account> {
  private static final Logger log = LoggerFactory.getLogger(AccountLoader.class);
  Path toRead;
  ParsistentAccountsDbInterface db;

  public AccountLoader(Path toRead, ParsistentAccountsDbInterface db) {
    this.toRead = toRead;
    this.db = db;
  }

  @Override
  public Account call() throws AccountAlreadyExists, ClassNotFoundException, IOException {
    log.info("Account loading - start");
    Account accountInDb;
    try (InputStream is = Files.newInputStream(toRead, StandardOpenOption.READ);
        BufferedInputStream bis = new BufferedInputStream(is);
        ObjectInputStream ois = new ObjectInputStream(is)) {
      Account accountFromDisk = (Account) ois.readObject();
      ois.close();
      //accountInDb = db.createAccount(accountFromDisk.getAccountId(), accountFromDisk.getOwner(), accountFromDisk.getBalance());
      accountInDb = db.put(accountFromDisk);
      log.debug("Reading of {} - done. Balance = {}", accountInDb.getAccountId(),
          accountInDb.getBalance());
    } catch (ClassNotFoundException | IOException ex) {
      log.error("Can't load account from {}: {}", toRead, ex);
      throw ex;
    }
    log.info("Account loading ({}) - end", accountInDb.getAccountId());
    return accountInDb;
  }

}
