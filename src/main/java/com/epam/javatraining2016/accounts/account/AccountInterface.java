package com.epam.javatraining2016.accounts.account;

import com.epam.javatraining2016.accounts.AccountOwner;
import com.epam.javatraining2016.accounts.InsufficientFundsException;

public interface AccountInterface {
  public void lock();

  public void unlock();

  public String getAccountId();

  public long getBalance();

  public void setBalance(long newBalance);

  void adjustBalance(long delta) throws InsufficientFundsException;

  public AccountOwner getOwner();
}
