package com.epam.javatraining2016.accounts.account;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.epam.javatraining2016.accounts.AccountOwner;
import com.epam.javatraining2016.accounts.InsufficientFundsException;
import com.epam.javatraining2016.accounts.base.ParsistentAccountsDbInterface;

public class Account implements AccountInterface, Serializable {
  private static final long serialVersionUID = -7188708459804759329L;
  transient ParsistentAccountsDbInterface db;
  private transient Lock lock;
  private String accountId;
  private AccountOwner owner;
  private long balance;

  public Account(ParsistentAccountsDbInterface db, String accountId, AccountOwner owner) {
    this.db = db;
    lock = new ReentrantLock();
    this.accountId = accountId;
    this.owner = owner;
    balance = 0;
  }

  @Override
  public void lock() {
    lock.lock();
  }

  @Override
  public void unlock() {
    lock.unlock();
  }

  @Override
  public long getBalance() {
    lock();
    long retval = balance;
    unlock();
    return retval;
  }

  @Override
  public void setBalance(long newBalance) {
    lock();
    try {
      balance = newBalance;
      db.write(this);
    } finally {
      unlock();
    }
  }

  @Override
  public AccountOwner getOwner() {
    return owner;
  }

  private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
    in.defaultReadObject();
    lock = new ReentrantLock();
  }

  @Override
  public String getAccountId() {
    return accountId;
  }

  @Override
  public void adjustBalance(long delta) throws InsufficientFundsException {
    lock();
    try {
      if (balance + delta < 0) {
        throw new InsufficientFundsException(String.format(
            "InsufficientFunds on account %s. Requested %d, have %d", accountId, -delta, balance));
      }
      balance += delta;
      db.write(this);
    } finally {
      unlock();
    }

  }


}
