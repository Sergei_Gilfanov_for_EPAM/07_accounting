package com.epam.javatraining2016.accounts;

import java.util.Comparator;

import com.epam.javatraining2016.accounts.account.AccountInterface;
import com.epam.javatraining2016.util.SimpleReentrantLock;

/*
 * Вспомогательный класс для блокировки пары счетов. Работает так, что всегда сначала блокируется
 * счет с меньшим accountId.
 */

public class LockPair implements SimpleReentrantLock {
  static private final Comparator<AccountInterface> comparator =
      (AccountInterface a, AccountInterface b) -> a.getAccountId().compareTo(b.getAccountId());
  AccountInterface first;
  AccountInterface second;

  public LockPair(AccountInterface a, AccountInterface b) {
    int compare = comparator.compare(a, b);
    // Особый случай. когда a и b совпадают, блочить нужно только один счет.
    if (compare == 0) {
      first = a;
      second = null;
    } else if (compare < 0) {
      first = a;
      second = b;
    } else {
      first = b;
      second = a;
    }
  }

  public void lock() {
    first.lock();
    if (second != null) {
      second.lock();
    }
  }

  public void unlock() {
    if (second != null) {
      second.unlock();
    }
    first.unlock();
  }
}

