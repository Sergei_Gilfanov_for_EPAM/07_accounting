package com.epam.javatraining2016.accounts.backgroundio;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epam.javatraining2016.accounts.AccountingException;
import com.epam.javatraining2016.accounts.account.Account;
import com.epam.javatraining2016.accounts.account.AccountLoader;
import com.epam.javatraining2016.accounts.base.AccountsDbLoaderInterface;
import com.epam.javatraining2016.accounts.base.ParsistentAccountsDbInterface;

public class AccountsBackgroundDbLoader implements AccountsDbLoaderInterface{
  private static final int THREADS_COUNT = 10;
  
  private static final Logger log = LoggerFactory.getLogger(AccountsBackgroundDbLoader.class);
  Path dirToLoad;

  public AccountsBackgroundDbLoader(Path dbDir) {
    dirToLoad = dbDir;
  }
  
  @Override
  public ConcurrentMap<String, Account> load(ParsistentAccountsDbInterface db) throws IOException {
    log.info("load - start");
    List<AccountLoader> loaders = createLoaders(db);
    List<Future<Account>> results = executeLoaders(loaders);
    collectResults(loaders, results);
    log.info("load - end");
    return null;
  }
  
  private List<AccountLoader> createLoaders(ParsistentAccountsDbInterface db) throws IOException {
    log.info("createLoaders - start");
    ArrayList<AccountLoader> retval = new ArrayList<AccountLoader>();
    try (DirectoryStream<Path> stream = Files.newDirectoryStream(dirToLoad, "*")) {
      for (Path accountFile : stream) {
        retval.add(new AccountLoader(accountFile, db));
      }
    } catch (IOException ex) {
      log.error("Can't scan {}: {}", dirToLoad, ex);
      throw ex;
    }
    log.info("createLoaders - end");
    return retval;
  }

  private List<Future<Account>> executeLoaders(List<AccountLoader> loaders) {
    log.info("executeLoaders - start");
    ExecutorService executor = Executors.newFixedThreadPool(THREADS_COUNT);
    // В случае исключения будем дальше работать с пустым списком
    List<Future<Account>> results = new ArrayList<Future<Account>>();
    try {
        results = executor.invokeAll(loaders);
    } catch (InterruptedException e) {
      // FIXME - игнорировать незавершенные задачи нехорошо
      e.printStackTrace();
    }
    executor.shutdown();
    
    while (!executor.isTerminated()) {
      try {
        executor.awaitTermination(1, TimeUnit.SECONDS);
      } catch (InterruptedException e) {
        // FIXME - игнорировать незавершенные задачи нехорошо
      }
    }
    log.info("executeLoaders - end");
    return results;
  }
  
  private void collectResults(List<AccountLoader> loaders, List<Future<Account>> results) {
    log.info("collectResults - start");
    int counter = 0;
    for (Future<Account> futureResult: results) {
      AccountLoader loader = loaders.get(counter++);
      try {
        futureResult.get();
      } catch (InterruptedException ex) {
        log.error("Thread for '{}' was interrupted", loader);
        continue;
      } catch (ExecutionException ex) {
        Throwable cause = ex.getCause();
        if (cause instanceof AccountingException ) {
          // Если нормальные ошибки по работе со счетом - то это нормальное сообщение пользователю.
          log.debug("{}: {}", loader, cause.getMessage());
        } else {
          log.error("Thread for '{}' Execution error. Cause: {}", loader, cause);
        }
        continue;
      }
      log.debug("{}: OK", loader);
    }
    log.info("collectResults - end");
  }

}
