package com.epam.javatraining2016.accounts.backgroundio;

import java.nio.file.Path;
import java.util.Comparator;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epam.javatraining2016.accounts.account.Account;
import com.epam.javatraining2016.accounts.account.AccountInterface;
import com.epam.javatraining2016.accounts.account.AccountWriter;
import com.epam.javatraining2016.accounts.base.AccountsDbWriterInterface;

public class AccountsBackgroundDbWriter implements Runnable, AccountsDbWriterInterface {
  private static final int THREADS_COUNT = 10;

  private static final Logger log = LoggerFactory.getLogger(AccountsBackgroundDbWriter.class);

  private static final Comparator<AccountInterface> accountsComparator =
      (AccountInterface a, AccountInterface b) -> a.getAccountId().compareTo(b.getAccountId());
  private boolean closed;
  // Очередь/буфер еще не записанных счетов
  private ConcurrentSkipListSet<Account> dirtyAccounts;
  Thread writerThread;
  ExecutorService executor;

  private Path writeWhere;

  public AccountsBackgroundDbWriter(Path writeWhere) {
    closed = false;
    dirtyAccounts = new ConcurrentSkipListSet<Account>(accountsComparator);

    this.writeWhere = writeWhere;
    executor = Executors.newFixedThreadPool(THREADS_COUNT);

    writerThread = new Thread(this, "Accounts Writer");
    writerThread.start();
  }

  @Override
  public void close() {
    log.info("Close - start");
    closed = true;
    while (!executor.isTerminated()) {
      try {
        executor.awaitTermination(1, TimeUnit.SECONDS);
      } catch (InterruptedException e) {
        ;// Игнорируем - нам нужно записать всю очередь работы
      }
    }
    log.info("Close - end");
  }

  @Override
  public void write(Account account) {
    if (closed) {
      throw new IllegalStateException("Write() on closing AccountsDbWriter");
    }
    dirtyAccounts.add(account);
    log.debug("Writing of '{}' queued", account.getAccountId());
  }

  @Override
  public void run() {
    log.info("Accounts writer - start");

    while (true) {
      Account dirtyAccount = dirtyAccounts.pollLast();
      if (dirtyAccount == null) {
        if (closed) {
          break;
        }
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          ; // Игнорируем - нам надо дозаписать всю очередь работы
        }
        continue;
      }
      Path accountFile = writeWhere.resolve(dirtyAccount.getAccountId());
      AccountWriter task = new AccountWriter(accountFile, dirtyAccount);
      executor.submit(task);
      log.debug("Account '{}' scheduled to Account writer", dirtyAccount.getAccountId());
      // FIXME игнорируем ошибки функции записи
    }
    executor.shutdown();
    log.info("Accounts writer - end");
  }

}
