package com.epam.javatraining2016.accounts.run;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epam.javatraining2016.accounts.AccountingException;
import com.epam.javatraining2016.accounts.base.AccountsDb;
import com.epam.javatraining2016.accounts.base.AccountsDbLoaderInterface;
import com.epam.javatraining2016.accounts.base.AccountsDbWriterInterface;
import com.epam.javatraining2016.accounts.base.ParsistentAccountsDbInterface;
import com.epam.javatraining2016.accounts.instantio.AccountsInstantDbLoader;
import com.epam.javatraining2016.accounts.nullio.AccountsNullDbWriter;
import com.epam.javatraining2016.util.Stat;

public class CalculateTotal {
  private static final String DB_PATH = "D:\\tmp\\accounts";

  private static final Logger log = LoggerFactory.getLogger(CalculateTotal.class);

  public static void main(String[] args)
      throws IOException, ClassNotFoundException, AccountingException {

    log.info("Main - start");
    long startTime = System.nanoTime();

    Path path = Paths.get(DB_PATH);
    AccountsDbLoaderInterface loader = new AccountsInstantDbLoader(path);
    AccountsDbWriterInterface writer = new AccountsNullDbWriter();
    ParsistentAccountsDbInterface accounts = new AccountsDb(loader, writer);
    long loadEndTime = System.nanoTime();
    
    Stat.printListAndTotal(accounts);
    accounts.close();
    
    log.info("Main - loading speed {} acct/sec",
        itemsSec(accounts.getSize(), loadEndTime - startTime));
  }

  private static long itemsSec(int count, long nanoSecDelta) {
    return (1000000000L * count) / nanoSecDelta;
  }

}
