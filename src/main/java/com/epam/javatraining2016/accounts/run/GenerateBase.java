package com.epam.javatraining2016.accounts.run;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;

import com.epam.javatraining2016.accounts.AccountAlreadyExists;
import com.epam.javatraining2016.accounts.AccountOwner;
import com.epam.javatraining2016.accounts.AccountingException;
import com.epam.javatraining2016.accounts.account.AccountInterface;
import com.epam.javatraining2016.accounts.backgroundio.AccountsBackgroundDbWriter;
import com.epam.javatraining2016.accounts.base.AccountsDb;
import com.epam.javatraining2016.accounts.base.AccountsDbLoaderInterface;
import com.epam.javatraining2016.accounts.base.AccountsDbWriterInterface;
import com.epam.javatraining2016.accounts.base.ParsistentAccountsDbInterface;
import com.epam.javatraining2016.accounts.nullio.AccountsNullDbLoader;

public class GenerateBase {
  private static final String DB_PATH = "D:\\tmp\\accounts";
  static final int COUNT = 50000;

  public static void main(String[] args)
      throws IOException, ClassNotFoundException, AccountingException {
    Path path = Paths.get(DB_PATH);
    AccountsDbLoaderInterface loader = new AccountsNullDbLoader();
    AccountsDbWriterInterface writer = new AccountsBackgroundDbWriter(path);
    ParsistentAccountsDbInterface accounts = new AccountsDb(loader, writer);

    Random random = new Random();

    for (int i = 0; i < COUNT; ++i) {
      AccountOwner owner = new AccountOwner("Owner " + i);
      AccountInterface account = null;
      try {
        account = accounts.createAccount(String.format("Acct_%010d", i), owner);
      } catch (AccountAlreadyExists e) {
        // Игнорируем - мы генерируем заведомо разные счета
      }
      long balance = random.nextInt(1000000);
      account.setBalance(balance);
    }
    accounts.close();
  }
}
