package com.epam.javatraining2016.accounts.run;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epam.javatraining2016.accounts.AccountingException;
import com.epam.javatraining2016.accounts.Transfer;
import com.epam.javatraining2016.accounts.backgroundio.AccountsBackgroundDbLoader;
import com.epam.javatraining2016.accounts.backgroundio.AccountsBackgroundDbWriter;
import com.epam.javatraining2016.accounts.base.AccountsDb;
import com.epam.javatraining2016.accounts.base.AccountsDbLoaderInterface;
import com.epam.javatraining2016.accounts.base.AccountsDbWriterInterface;
import com.epam.javatraining2016.accounts.base.ParsistentAccountsDbInterface;
import com.epam.javatraining2016.util.Stat;

public class RandomTransfers {
  private static final String DB_PATH = "D:\\tmp\\accounts";
  private static final String TEST_ACCOUNT = "Acct_0000000001";
  private static final int TRANSFERS_COUNT = 100000;
  private static final int THREADS_COUNT = 100;

  private static final Logger log = LoggerFactory.getLogger(RandomTransfers.class);

  public static void main(String[] args)
      throws IOException, ClassNotFoundException, AccountingException {
    log.info("Main - start");
    long startTime = System.nanoTime();
    
    Path path = Paths.get(DB_PATH);
    AccountsDbLoaderInterface loader = new AccountsBackgroundDbLoader(path);
    AccountsDbWriterInterface writer = new AccountsBackgroundDbWriter(path);
    ParsistentAccountsDbInterface accounts = new AccountsDb(loader, writer);
    long loadEndTime = System.nanoTime();
    log.info("Main - loading speed {} acct/sec", itemsSec(accounts.getSize(), loadEndTime - startTime) );
    
    Stat.printTotal(accounts);
    long total1EndTime = System.nanoTime();
    log.info("Main - aggregation speed {} acct/sec", itemsSec(TRANSFERS_COUNT, total1EndTime - loadEndTime) );
        
    randomTransfers(TRANSFERS_COUNT, TEST_ACCOUNT, accounts);
    long transfersEndTime = System.nanoTime();
    
    Stat.printTotal(accounts);
    long total2EndTime = System.nanoTime();
    log.info("Main - aggregation speed {} acct/sec", itemsSec(TRANSFERS_COUNT, total2EndTime - transfersEndTime) );
    
    long endTime = System.nanoTime();
    log.info("Main - end. Speed {} transf/sec", itemsSec(TRANSFERS_COUNT, endTime - startTime) );
  }

  static void randomTransfers(int transfersCount, String testAccount,
      ParsistentAccountsDbInterface accounts) {
    log.info("randomTransfers - start");
    long startTime = System.nanoTime();
    ArrayList<Transfer> transfers = createTransfers(transfersCount, testAccount, accounts);
    List<Future<Transfer>> results = doTransfers(transfers);
    long dirtyEndTime = System.nanoTime();
    log.info("randomTransfers - dirty end. Speed {} trans/sec", itemsSec(transfersCount, dirtyEndTime - startTime));
    accounts.close();
    long cleanEndTime = System.nanoTime();
    log.info("randomTransfers - clean end. Speed {} trans/sec", itemsSec(transfersCount, cleanEndTime - startTime));

    outputResults(transfers, results);
    long endTime = System.nanoTime();
    log.info("randomTransfers - end. Speed {}", itemsSec(TRANSFERS_COUNT, endTime - startTime));
  }

  private static ArrayList<Transfer> createTransfers(int transferCount, String testAccount,
      ParsistentAccountsDbInterface accounts) {
    log.info("createTransfers - start");
    long startTime = System.nanoTime();
    Random random = new Random();
    int acctitUpperLimit = accounts.getSize() * 105 / 100; // + 5% 
    ArrayList<Transfer> transfers = new ArrayList<Transfer>(transferCount);
    for (int i = transferCount; i != 0; --i) {
      String second = String.format("Acct_%010d", random.nextInt(acctitUpperLimit));
      long amount = -500000 + random.nextInt(1000000);
      transfers.add(new Transfer(accounts, testAccount, second, amount));
    }
    long endTime = System.nanoTime();
    log.info("createTransfers - end. Speed {} transf/sec", itemsSec(transferCount, endTime - startTime));
    return transfers;
  }

  private static List<Future<Transfer>> doTransfers(ArrayList<Transfer> transfers) {
    log.info("doTransfers - start");
    long startTime = System.nanoTime();
    ExecutorService executor = Executors.newFixedThreadPool(THREADS_COUNT);
    // В случае исключения будем дальше работать с пустым списком
    List<Future<Transfer>> results = new ArrayList<Future<Transfer>>();
    try {
      results = executor.invokeAll(transfers);
    } catch (InterruptedException e) {
      // FIXME - игнорировать незавершенные задачи нехорошо
      e.printStackTrace();
    }
    executor.shutdown();

    while (!executor.isTerminated()) {
      try {
        executor.awaitTermination(1, TimeUnit.SECONDS);
      } catch (InterruptedException e) {
        // FIXME - игнорировать незавершенные задачи нехорошо
      }
    }
    long endTime = System.nanoTime();
    log.info("doTransfers - end. Speed {} transf/sec", itemsSec(transfers.size(), endTime - startTime));
    return results;
  }


  private static void outputResults(ArrayList<Transfer> transfers, List<Future<Transfer>> results) {
    log.info("outputResults - start");
    long startTime = System.nanoTime();
    int counter = 0;
    for (Future<Transfer> futureResult : results) {
      Transfer transfer = transfers.get(counter++);
      try {
        futureResult.get();
      } catch (InterruptedException ex) {
        log.error("Thread for '{}' was interrupted", transfer);
        continue;
      } catch (ExecutionException ex) {
        Throwable cause = ex.getCause();
        if (cause instanceof AccountingException) {
          // Если нормальные ошибки по работе со счетом - то это нормальное сообщение пользователю.
          log.debug("{}: {}", transfer, cause.getMessage());
        } else {
          log.error("Thread for '{}' Execution error. Cause: {}", transfer, cause);
        }
        continue;
      }
      log.debug("{}: OK", transfer);
    }
    long endTime = System.nanoTime();
    log.info("outputResults - end. Speed {} transf/sec", itemsSec(transfers.size(), endTime - startTime));
  }

  private static long itemsSec(int count, long nanoSecDelta) {
    return (1000000000L * count)/nanoSecDelta;
  }


}
