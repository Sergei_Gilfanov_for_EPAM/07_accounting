package com.epam.javatraining2016.accounts;

import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epam.javatraining2016.accounts.account.AccountInterface;
import com.epam.javatraining2016.accounts.base.AccountsDbInterface;

public class Transfer implements Callable<Transfer> {
  private static final Logger log = LoggerFactory.getLogger(Transfer.class);

  AccountsDbInterface db;
  String from;
  String to;
  long amount;

  public Transfer(AccountsDbInterface db, String from, String to, long amount) {
    this.db = db;
    this.from = from;
    this.to = to;
    this.amount = amount;
  }

  @Override
  public Transfer call() throws AccountDoesNotExists, InsufficientFundsException {
    AccountInterface fromAccount = null;
    AccountInterface toAccount = null;
    log.info("Transfer {} from {} to {} - start", amount, from, to);
    try {
      fromAccount = db.getAccount(from);
      toAccount = db.getAccount(to);
    } catch (AccountDoesNotExists ex) {
      log.debug("Account does not exists");
      throw ex;
    }

    LockPair lock = new LockPair(fromAccount, toAccount);
    try {
      lock.lock();
      long oldBalance = fromAccount.getBalance();
      fromAccount.adjustBalance(-amount);
      // Если что-нибудь пойдет не так с обновлением второго, то сумму на первом надо вернуть.
      try {
        toAccount.adjustBalance(amount);
      } catch (Exception ex) {
        fromAccount.setBalance(oldBalance);
        throw ex;
      }
    } finally {
      lock.unlock();
    }
    log.info("Transfer {} from {} to {} - end", amount, from, to);
    return this;
  }

  public String toString() {
    return String.format("Transfer %d from %s to %s", amount, from, to);
  }
}

