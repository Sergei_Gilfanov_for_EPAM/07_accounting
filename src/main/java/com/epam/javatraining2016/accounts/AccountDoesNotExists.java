package com.epam.javatraining2016.accounts;

public class AccountDoesNotExists extends AccountingException {
  private static final long serialVersionUID = -5419959276968061961L;

  public AccountDoesNotExists(String msg) {
    super(msg);
  }
}
