package com.epam.javatraining2016.accounts;

public class InsufficientFundsException extends AccountingException {
  private static final long serialVersionUID = 9030518969177054934L;

  public InsufficientFundsException(String msg) {
    super(msg);
  }
}
