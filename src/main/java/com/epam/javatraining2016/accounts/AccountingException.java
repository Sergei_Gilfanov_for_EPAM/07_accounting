package com.epam.javatraining2016.accounts;

public class AccountingException extends Exception {
  private static final long serialVersionUID = 831013445383100419L;

  public AccountingException(String msg) {
    super(msg);
  }
}
