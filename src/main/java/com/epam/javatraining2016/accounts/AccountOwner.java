package com.epam.javatraining2016.accounts;

import java.io.Serializable;

public class AccountOwner implements Serializable {
  private static final long serialVersionUID = 6777538783999887539L;

  String name;

  public AccountOwner(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }
}
